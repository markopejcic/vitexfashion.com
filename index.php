<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 9/5/18
 * Time: 11:57 PM
 */


class GalleryManager {
    private $galleryName;
    private $isIndex;
    private $prepend;
    private $seasons;
    private $data;

    public function __construct() {
        $this->galleryName = 'gallery';
        $this->isIndex = !isset($_SERVER['REDIRECT_URL']);
        $this->prepend = $this->isIndex ? '' : '..\\..\\';
        $this->setSeasons();
        $this->setData();
    }

    private function setSeasons() {
        $gallery = scandir($this->galleryName, SCANDIR_SORT_ASCENDING);
        $seasons = array_filter($gallery, function($v, $k) {
            return $v[0] != '.' && $v[0] != '_' && $v != 'index.php' && $v != 'index.html' && $v != 'styles.css';
        }, ARRAY_FILTER_USE_BOTH);

        $folders = Array();
        foreach ($seasons as $season) {
            $folders[$season] = "$this->prepend$this->galleryName\\$season";
        }

        $this->seasons = $folders;
    }

    private function setData() {
        $data = Array();
        foreach ($this->getSeasons() as $season => $link ) {
            $dir = scandir($this->galleryName.DIRECTORY_SEPARATOR.$season, SCANDIR_SORT_ASCENDING);
            $files = array_filter($dir, function($v, $k) {
                return $v[0] != '.' && $v[0] != '_' && $v != 'index.php' && $v != 'index.html' && $v != 'styles.css';
            }, ARRAY_FILTER_USE_BOTH);
            $data[$season] = $files;
        }

        $arr = array_map('array_values', $data);
        $this->data = $arr;
    }

    public function getSeasons() {
        return $this->seasons;
    }

    public function seasonExists($season) {
        return isset($this->data[$season]);
    }

    public function getData($forSeason = '') {
        if (empty($forSeason)) {
            return $this->data;
        } else {
            if (isset($this->data[$forSeason])) {
                return $this->data[$forSeason];
            } else {
                return Array();
            }
        }
    }
}


$gallery_url_name = 'katalozi';
$gallery_name = 'gallery';
$is_index = !isset($_SERVER['REDIRECT_URL']);
$prepend = $is_index ? '' : '../../';

//if ($is_index) {
//
//    die();
//}

$params = Array();
if (isset($_SERVER['REDIRECT_URL'])) {
    $params = explode('/', $_SERVER['REDIRECT_URL']);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>ViTexfashion.com - Proizvodnja Dečije garderobe - Ul. Vinogradska 150B -
        11000 Beograd </title>
    <meta name="keywords" content="ViTexfashion.com - Proizvodnja Dečije garderobe - Ul. Vinogradska 150B - 11000 Beograd  tel: 011/226-91-45 011/226-91-46  mail: office@vitexfashion.com">
    <meta name="description" content="ViTexfashion.com - Proizvodnja Dečije garderobe - Ul. Vinogradska 150B - 11000 Beograd  tel: 011/226-91-45 011/226-91-46  mail: office@vitexfashion.com">
    <link rel="stylesheet" type="text/css" href="/styles.css">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <link href="https://unpkg.com/nanogallery2/dist/css/nanogallery2.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://unpkg.com/nanogallery2/dist/jquery.nanogallery2.min.js"></script>
</head>
<body>

    <div id="traka">
        <div id="traka-naslov">
            VITEX
        </div>
    </div>
    <div id="strana">

<?php

$galleryManager = new GalleryManager();
$folders = $galleryManager->getSeasons();
$data = $galleryManager->getData();

if (isset($params[0])) {

    $season = $params[1];
    $file = @$params[2];

    // show one season
    if (isset($season) && !empty($season)) {
        if (
            !array_key_exists($season, $data)
            || count($params) > 3
            || (count($params) == 3 && !empty($file) && !in_array($file, $data[$season]))
        ) {
            echo '<h1>WROGN URL!</h1>';
            die();
        }

        echo "<center><h1>$season</h1></center>";

        ?>
        <div data-nanogallery2='{
        "itemsBaseURL": "",
        "thumbnailWidth": "200",
        "thumbnailLabel": {
          "display": false
        },
        "thumbnailHoverEffect2": "scale120",
        "thumbnailAlignment": "center"
      }'>
            <?php

            foreach ($data[$season] as $element) {
                $link = $prepend . $gallery_name . '/' . $season . '/' . $element;
                echo "<a href=\"$link\" data-ngthumb=\"$link\" data-ngdesc=\"\">$element</a><br />";
            }

            ?>
        </div>

        <div>
            <center>
                <h3>Pogledajte ostale kataloge</h3>
                <div>
                    <?php
                    foreach ($data as $folder_name => $file) {
                        echo "<a href=\"$prepend/$folder_name\">$folder_name</a> | ";
                    }
                    echo "<br><a href=\"$prepend\">Povratak na pocetnu stranu</a><br />";
                    ?>
                </div>
            </center>
        </div>

        <?php

    }
} else { // show list of seasons
    ##### KATALOZI #####
    ?>
    <div id="doborsli-text">
        Dobrodošli - Pogledajte naše kataloge
    </div>

    <div class="gallery" id="galerija">
        <?php
        foreach ($data as $folder_name => $file) {

            $ime_nalova = in_array('naslovna', $file, false) ? array_search('naslovna', $file, false)[0] : '';
            $prva_slika = (count($file) > 0) ? $prepend . $gallery_name . '/' . $folder_name . '/' . $file[0] : '';

            $ime_slike = empty($ime_nalova) ? $prva_slika : $ime_nalova;

            $link_slike = (count($file) > 0) ? '/' . $ime_slike : "";
            ?>
            <div class="gallery">
                <a href="<?php echo $prepend . '/' . $folder_name ?>">
                    <img src="<?php echo $link_slike ?>" alt="<?php echo $folder_name ?>" width="300" height="200">
                </a>
                <div class="desc"><?php echo $folder_name ?></div>
            </div>
            <?php
        }
        ?>

    </div>

    <p id="info-text">
        ﻿Vitex je preduzeće koje se bavi proizvodnjom i prodajom dečije garderobe za decu uzrasta (0-16) godina.
        Preduzeće posluje u ovoj oblasti već 3 decenije i svoj dug vek poslovanja je stekao kvalitetnim proizvodima
        koji uvek prate trendove. Naše preduzeće ima sopstveni dizajnerski studio, kao i proizvodnju u kojoj se
        nalaze: krojačnica, prerada, štamparija itd. Mi pravimo dečije kolekcije koje su dizajnerski raznovrsne u
        velikom izboru boja, izrađujemo ih od kvalitetnih materijala kako bi se deca osećala prijatno dok nose
        proizvode iz naše kolekcije. Da je naša odeća popularna među decom i roditeljima govori velika portažnja
        naših proizvoda putem naših distributera širom Srbije. Postanite i vi distributer naših kolekcija, ostvarimo
        saradnju zarad obostranog zadovoljstva.
    </p>
    <?php
}

?>
    </div>

    <p align="center">
        <b id="tekst-dno">
            ViTex -
            Proizvodnja i veleprodaja Decije garderobe - Ul. Vinogradska
            150B - 11000 Beograd <br>
            tel: 011/226-91-45 011/226-91-46 mail: office@vitexfashion.com
        </b>
    </p>

</body>
</html>
